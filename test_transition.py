from jira import JIRA, JIRAError

jira = JIRA(
    options={"server": "https://armeltakou.atlassian.net"},
    basic_auth=("armeltakou@gmail.com", "TM7j6l9DAvtosJrYa2ZH4B59"),
)


def test_in_progress():
    issue = jira.issue("MX-1")

    try:
        jira.transition_issue(issue, "ANALYSE")
    except JIRAError:
        assert issue.fields.status.name == "A FAIRE"
        return
    assert issue.fields.status.name == "ANALYSE"
