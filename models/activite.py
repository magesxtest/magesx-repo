# -*- coding: utf-8 -*-

from odoo import fields, models

class IotActivite(models.Model):
    _name = "iot.activite"
    _description = "Les données des activités collectées des différents appareils connectés"


    #idactivite = fields.Integer(string="ID Activité", default=1)
    patient_id = fields.Many2one("iot.utilisateurs", string="ID Patient")
    calorie = fields.Float(string="Kilo Calorie dépensé")
    date = fields.Datetime(string="Date activité")
    deepsleep = fields.Float(string="Sommeil Profond")
    distance = fields.Integer(string="Distance")
    shallowsleep = fields.Float(string="Sommeil Legé")
    sleepEndTime = fields.Float(string="Heure de fin de Sommeil")
    startSleep = fields.Float(string="Heure de debut de Sommeil")
    step = fields.Integer(string="Nombre de pas")
    timeZone = fields.Integer(string="fuseau horaire")
    totalSleeptime = fields.Integer(string="Temps de sommeil")
    poids = fields.Float(string="Poids")
    stepGoal = fields.Integer(string="Nombre de pas à atteindre")
    kcalorieGoal = fields.Float(string="Kilo Calorie à atteindre")
    distanceGoal = fields.Integer(string="Distance à atteindre") 
    
    def name_get(self):
        res=[]
        for field in self:
            res.append((field.id, ('Activité #' + str(field.id) + ' du ' + str(field.dateactivite))))           
        return res
    