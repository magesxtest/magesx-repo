# -*- coding: utf-8 -*-
{
    'name': "iOT Project",

    'summary': """
        Application de traitement des données patient en vue d'une prise
        de décision par le médecin traitant""",

    'description': """
        Application de traitement des données patient en vue d'une prise de décision par le médecin traitant
    """,

    'author': "NEW BASE TECHNOLOGIES",
    'website': "http://www.newbasetechnologies.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/ir_con_iot.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/menu.xml',
        'views/utilisateur.xml',
        'views/connecteur.xml',
        'views/activite.xml',
        'views/res_users_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}